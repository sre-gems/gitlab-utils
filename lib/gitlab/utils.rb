#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'logger'

module Gitlab
  # Module utils
  module Utils
    class << self
      attr_accessor :logger

      Dir[File.join(File.dirname(__FILE__), '*', '*.rb')].each do |file|
        require file
      end
      def start
        Gitlab::Utils::Cli.start(ARGV)
      rescue => error
        puts error.message
        puts error.backtrace
        exit 1
      end
    end
    # Initialize the base logger
    Gitlab::Utils.logger = Logger.new(STDOUT)
  end
end
