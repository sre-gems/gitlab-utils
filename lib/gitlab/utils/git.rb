#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# For $CHILD_STATUS
require 'English'

module Gitlab
  module Utils
    # Git wrapper
    class Git
      attr_reader :last_out

      def initialize(path = nil)
        @cmd = 'git'
        @path = path
        @last_out = nil
      end

      def exist?
        File.directory?(@path) && (status == :ok)
      end

      def command(*args)
        command_path(@path, args)
      end

      def command_path(path, *args)
        cmd = @cmd + (' ' + args.join(' '))
        Dir.chdir(path) { @last_out = `#{cmd} 2>&1` }
        $CHILD_STATUS.exitstatus == 0 ? :ok : :error
      end

      def method_missing(method, *args)
        cmd = method.to_s.tr('_', '-').chomp('!')
        status = respond_to?(cmd) ? send(cmd, args) : command(*([cmd] + args))
        if method[-1] == '!'
          raise @last_out if status == :error
          last_out
        else
          status
        end
      end

      # clone is a particular command and should be implemented separately
      def clone(*args, url)
        path = File.expand_path('..', @path)
        dir = File.basename(@path)
        command_path(path, 'clone', *args, url, dir)
      end
    end
  end
end
