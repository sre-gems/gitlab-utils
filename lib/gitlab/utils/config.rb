#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'yaml'

module Gitlab
  # Module Utils
  module Utils
    # Config
    module Config
      class << self
        MANDATORY_KEYS = %w(token endpoint).freeze

        def load(options, mandatory = [])
          config = read_config(options['config_file'])
          config = config.merge(options)
          check_missing(config, mandatory)
          process_profiles(config)
        end

        def read_config(config_file)
          YAML.load_file config_file if File.exist? config_file
        rescue StandardError => e
          raise "#{e}\nCould not load configuration file: #{config_file}"
        end

        def check_missing(config, mandatory)
          missing = (MANDATORY_KEYS + mandatory).map do |key|
            config[key].nil? ? key : nil
          end.compact

          return config if missing.empty?
          puts "Please provide valid #{missing.join(', ')}"
          exit 1
        end

        def process_profiles(config)
          default = config.dig('profiles', 'default')
          config['profiles']&.each_pair do |name, profile|
            next if name == 'default'
            config['profiles'][name] = deep_merge(default, profile)
          end
          config
        end

        def deep_merge(hash1, hash2)
          merger = proc do |_key, v1, v2|
            if v1.is_a?(Hash) && v2.is_a?(Hash)
              v1.merge(v2, &merger)
            else
              v2
            end
          end
          hash1.merge(hash2, &merger)
        end
      end
    end
  end
end
