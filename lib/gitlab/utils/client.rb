#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'rest-client'
require 'json'

module Gitlab
  # Module Utils
  module Utils
    # Class Client
    class Client
      def initialize(endpoint, token)
        @endpoint = endpoint
        @token = token
      end

      def get(action, options = {})
        results = []
        page = 1
        loop do
          response = get_response(action, options.merge(page: page))
          pages = response.headers[:x_total_pages].to_i
          results << parse_json(response.body)
          return results.last if pages == 0
          break if (page += 1) > pages
        end
        results.flatten
      end

      def put(path, options)
        call(:put, path, options)
      end

      def post(path, options)
        call(:post, path, options)
      end

      def delete(path, options)
        call(:delete, path, options)
      end

      def groups_projects(groupid, archived = false)
        group_projects = {}
        projects = get("groups/#{groupid}/projects", archived: archived)
        projects.each do |project|
          group_projects[project['name']] = project
        end
        group_projects
      end

      private

      def call(method, path, options)
        args = ["#{@endpoint}/#{path}", fill(options)]
        response = RestClient.send(method, *args)
        check_response(response, path)
        parse_json(response.body)
      end

      def get_response(action, options)
        options = { params: fill(options) }
        response = RestClient.get("#{@endpoint}/#{action}", options)
        check_response(response, action)
      end

      def fill(options)
        options.merge(private_token: @token, per_page: 1)
      end

      def check_response(response, action)
        unless [200, 201].include?(response.code)
          raise "Error in request: #{action}, Code: #{response.code}"
        end
        response
      end

      def parse_json(string)
        JSON.parse(string)
      rescue JSON::ParserError
        string
      end
    end
  end
end
